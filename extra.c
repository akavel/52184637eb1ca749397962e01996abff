
layer_state_t layer_state_set_user(layer_state_t state) {
    switch (get_highest_layer(state)) {
        case 0:
            /* set_led_off; */
            blu_led_off;
            grn_led_off;
            red_led_off;
            break;
        case 1:
            blu_led_on;
            grn_led_off;
            red_led_off;
            break;
        case 2:
            blu_led_off;
            grn_led_on;
            red_led_off;
            break;
        case 3:
            blu_led_off;
            grn_led_off;
            red_led_on;
            break;
        default:
            break;
    }
  return state;
}

bool led_update_user(led_t led_state)
{
    if (led_state.caps_lock) {
        wht_led_on;
    } else {
        wht_led_off;
    }
    return false;
}

